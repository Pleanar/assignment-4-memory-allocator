#include <assert.h>

#include "mem.h"
#include "mem_internals.h"

#define TEST_HEAP_SIZE 4096

static struct block_header* header_from_ptr(void* ptr) {
    return (struct block_header*)((uint8_t*)ptr - offsetof(struct block_header, contents));
}

static inline void print_delimiter(){
    printf("---------------------\n");
}

static void print_prologue(const char* text){
    print_delimiter();
    printf("%s", text);
    printf("\n\n");
}

static void print_epilogue(){
    print_delimiter();
    printf("\n");
}

void test_successful_memory_allocation(){
    printf("Testing memory allocation...\n\n");

    print_prologue("Heap creation...");
    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Allocating 1 block...");
    void* allocated_block = _malloc(123);
    assert(allocated_block != NULL);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Releasing 1 block...");
    _free(allocated_block);
    debug_heap(stdout, heap);
    print_epilogue();

    printf("Terminating heap...\n");
    heap_term();
    printf("Memory allocation passed.\n\n");
}

void test_release_one_of_several_blocks(){
    printf("Testing release of one of several blocks...\n\n");

    print_prologue("Heap creation...");
    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Allocating 2 blocks...");
    void* allocated_block1 = _malloc(123);
    void* allocated_block2 = _malloc(456);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Releasing 1 of them...");
    _free(allocated_block2);
    debug_heap(stdout, heap);
    print_epilogue();

    __attribute__((unused)) struct block_header* block1 = header_from_ptr(allocated_block1);
    __attribute__((unused)) struct block_header* block2 = header_from_ptr(allocated_block2);

    assert(block1->next == block2);
    assert(block2->is_free);

    printf("Releasing the other...\n");
    _free(allocated_block1);
    printf("Terminating heap...\n");
    heap_term();
    printf("Release of one of several blocks passed.\n\n");
}

void test_release_two_of_several_blocks(){
    printf("Testing release of two of several blocks...\n\n");

    print_prologue("Heap creation...");
    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Allocating 4 blocks...");
    void* allocated_block1 = _malloc(123);
    void* allocated_block2 = _malloc(456);
    void* allocated_block3 = _malloc(789);
    void* allocated_block4 = _malloc(1011);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Releasing 2 of them...");
    _free(allocated_block2);
    _free(allocated_block4);
    debug_heap(stdout, heap);
    print_epilogue();

    __attribute__((unused)) struct block_header* block1 = header_from_ptr(allocated_block1);
    __attribute__((unused)) struct block_header* block3 = header_from_ptr(allocated_block3);

    assert(!block1->is_free);
    assert(!block3->is_free);

    printf("Releasing the others...\n");
    _free(allocated_block1);
    _free(allocated_block3);
    printf("Terminating heap...\n");
    heap_term();
    printf("Release of two blocks out of several test passed\n\n");
}

void test_expand_existing_region(){
    printf("Testing existing region expansion...\n\n");

    print_prologue("Heap creation...");
    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    print_epilogue();

    __attribute__((unused)) size_t initial_region_size = ((struct region*) heap)->size;

    print_prologue("Allocating big block...");
    void* allocated_block1 = _malloc(5 * TEST_HEAP_SIZE);
    __attribute__((unused)) struct block_header* block1 = header_from_ptr(allocated_block1);
    debug_heap(stdout, heap);
    print_epilogue();

    __attribute__((unused)) size_t after_expansion_region_size = ((struct region*) heap)->size;

    assert(after_expansion_region_size > initial_region_size);
    assert(block1);
    assert(block1->capacity.bytes >= 5 * TEST_HEAP_SIZE);



    printf("Releasing the block...\n");
    _free(allocated_block1);
    printf("Terminating heap...\n");
    heap_term();
    printf("Existing region expansion test passed\n\n");
}

void test_allocate_new_region(){
    printf("Testing new region allocation...\n\n");

    print_prologue("Heap creation...");
    void* heap = heap_init(TEST_HEAP_SIZE);
    assert(heap != NULL);
    debug_heap(stdout, heap);
    print_epilogue();

    print_prologue("Hole creation...");\
    void* hole = mmap(HEAP_START + REGION_MIN_SIZE, 1213, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(hole == HEAP_START + REGION_MIN_SIZE);
    printf("Allocating big block...\n");

    void* new_block = _malloc(2*REGION_MIN_SIZE);
    __attribute__((unused)) struct block_header* new_block_header = header_from_ptr(new_block);
    debug_heap(stdout, heap);
    print_epilogue();

    assert(new_block != NULL);
    assert(new_block != hole);
    assert(new_block_header->capacity.bytes >= 2*REGION_MIN_SIZE);

    printf("Releasing memory of hole...\n");
    munmap(hole, 100);
    printf("Releasing the block...\n");
    _free(new_block);
    printf("Terminating heap...\n");
    heap_term();
    printf("Allocation in a new memory region test passed\n\n");
}

int main() {

    print_prologue("Test 1:");
    test_successful_memory_allocation();
    print_epilogue();

    print_prologue("Test 2:");
    test_release_one_of_several_blocks();
    print_epilogue();

    print_prologue("Test 3:");
    test_release_two_of_several_blocks();
    print_epilogue();

    print_prologue("Test 4:");
    test_expand_existing_region();
    print_epilogue();

    print_prologue("Test 5:");
    test_allocate_new_region();
    print_epilogue();

    return 0;
}